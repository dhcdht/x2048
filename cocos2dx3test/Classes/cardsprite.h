#ifndef CARDSPRITE_H
#define CARDSPRITE_H

#include "cocos2d.h"

class CardSprite : public cocos2d::Sprite
{
public:
    static CardSprite* createCardSprite(int aNumber, int aWidth, int aHeight, float aSpriteX, float aSpriteY);
    virtual bool init();
    CREATE_FUNC(CardSprite);

    int getNumber();
    void setNumber(int aNumber);

private:
    int m_iNumber;
    
    void privateInit(int aNumber, int aWidth, int aHeight, float aSpriteX, float aSpriteY);

    cocos2d::LabelTTF* m_pLabelNumber;
    cocos2d::LayerColor* m_pLayerColor;
};

#endif // CARDSPRITE_H
