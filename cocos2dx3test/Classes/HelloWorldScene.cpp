#include "HelloWorldScene.h"

USING_NS_CC;

static int kMinDistance = 5;
static int kCardSize = 50;

Scene* HelloWorld::createScene()
{
    // 'scene' is an autorelease object
    auto scene = Scene::create();
    
    // 'layer' is an autorelease object
    auto layer = HelloWorld::create();

    // add layer as a child to scene
    scene->addChild(layer);

    // return the scene
    return scene;
}

// on "init" you need to initialize your instance
bool HelloWorld::init()
{
    //////////////////////////////
    // 1. super init first
    if ( !Layer::init() )
    {
        return false;
    }
    
    m_iScore = 0;
    
    Size visibleSize = Director::getInstance()->getVisibleSize();
//    Point origin = Director::getInstance()->getVisibleOrigin();
    
    LayerColor *bgColor = LayerColor::create(Color4B::GRAY, visibleSize.width, visibleSize.height);
    this->addChild(bgColor);
    
    m_pLabelScore = LabelTTF::create();
    m_pLabelScore->setFontSize(50);
    m_pLabelScore->setFontFillColor(Color3B::BLACK);
    m_pLabelScore->setString(String::createWithFormat("Score : %i", m_iScore)->getCString());
    this->addChild(m_pLabelScore);
    
//    CardSprite *card = CardSprite::createCardSprite(2, 100, 100, 100, 200);
//    this->addChild(card);
    int xMargin, yMargin;
    
    this->createCardSprite(visibleSize, &xMargin, &yMargin);
    
    if (xMargin > yMargin)
    {
        m_pLabelScore->setPosition(xMargin/2, visibleSize.height/2);
    }
    else
    {
        m_pLabelScore->setPosition(visibleSize.width/2, yMargin/2);
    }
    
    this->autoCreateCardNumber();
    this->autoCreateCardNumber();

    auto touchListener = EventListenerTouchOneByOne::create();
    touchListener->onTouchBegan = CC_CALLBACK_2(HelloWorld::onTouchBegan, this);
    touchListener->onTouchEnded = CC_CALLBACK_2(HelloWorld::onTouchEnded, this);
    this->getEventDispatcher()->addEventListenerWithSceneGraphPriority(touchListener, this);
//    this->getEventDispatcher()->setEnabled(true);
    
    return true;
}

bool HelloWorld::onTouchBegan(Touch *touch, Event *unused_event)
{
    Point touchPoint = touch->getLocation();
    beginX = touchPoint.x;
    beginY = touchPoint.y;

    return true;
}

void HelloWorld::onTouchEnded(Touch *touch, Event *unused_event)
{
    Point touchPoint = touch->getLocation();
    endX = touchPoint.x;
    endY = touchPoint.y;

    int distanceX = endX - beginX;
    int distanceY = endY - beginY;
    bool isMovedFlag = false;
    int score = 0;
    if (abs(distanceX) >= abs(distanceY))
    {
        if (distanceX > kMinDistance)
        {
            isMovedFlag = this->doRight(&score);
        }
        else if (distanceX < -kMinDistance)
        {
            isMovedFlag = this->doLeft(&score);
        }
    }
    else
    {
        if (distanceY > kMinDistance)
        {
            isMovedFlag = this->doUp(&score);
        }
        else if (distanceY < -kMinDistance)
        {
            isMovedFlag = this->doDown(&score);
        }
    }
    if (isMovedFlag)
    {
        m_iScore += score;
        
        m_pLabelScore->setString(String::createWithFormat("Score : %i", m_iScore)->getCString());
        log("%i", m_iScore);
        
        this->autoCreateCardNumber();
    }
    
    if (this->isGameOver())
    {
        log("Game Over");
    }
}

bool HelloWorld::doUp(int *outScore)
{
    log("doUp");
    
    bool isMovedFlag = false;
    int score = 0;
    
    for (int j = 0; j < MapSize; j++)
    {
        for (int i = 0; i < MapSize-1; i++)
        {
            CardSprite *curCard = m_pCardArray[i][j];
            for (int m = i+1; m < MapSize; m++)
            {
                CardSprite *difCard = m_pCardArray[m][j];
                if (0 != difCard->getNumber())
                {
                    if (0 == curCard->getNumber())
                    {
                        curCard->setNumber(difCard->getNumber());
                        difCard->setNumber(0);
                        
                        isMovedFlag = true;
                    }
                    else if (difCard->getNumber() == curCard->getNumber())
                    {
                        curCard->setNumber(2 * curCard->getNumber());
                        difCard->setNumber(0);
                        
                        score += curCard->getNumber();
                        
                        isMovedFlag = true;
                        
                        break;
                    }
                    else
                    {
                        break;
                    }
                }
            }
        }
    }
    
    *outScore = score;
    
    return isMovedFlag;
}

bool HelloWorld::doDown(int *outScore)
{
    log("doDown");
    
    bool isMovedFlag = false;
    int score = 0;
    
    for (int j = 0; j < MapSize; j++)
    {
        for (int i = MapSize-1; i > 0; i--)
        {
            CardSprite *curCard = m_pCardArray[i][j];
            for (int m = i-1; m >= 0; m--)
            {
                CardSprite *difCard = m_pCardArray[m][j];
                if (0 != difCard->getNumber())
                {
                    if (0 == curCard->getNumber())
                    {                        
                        curCard->setNumber(difCard->getNumber());
                        difCard->setNumber(0);
                        
                        isMovedFlag = true;
                    }
                    else if (difCard->getNumber() == curCard->getNumber())
                    {
                        curCard->setNumber(2 * curCard->getNumber());
                        difCard->setNumber(0);
                        
                        score += curCard->getNumber();
                        
                        isMovedFlag = true;
                        
                        break;
                    }
                    else
                    {
                        break;
                    }
                }
            }
        }
    }
    
    *outScore = score;
    
    return isMovedFlag;
}

bool HelloWorld::doLeft(int *outScore)
{
    log("doLeft");
    
    bool isMovedFlag = false;
    int score = 0;
    
    for (int i = 0; i < MapSize; i++)
    {
        for (int j = 0; j < MapSize-1; j++)
        {
            CardSprite *curCard = m_pCardArray[i][j];
            for (int m = j+1; m < MapSize; m++)
            {
                CardSprite *difCard = m_pCardArray[i][m];
                if (0 != difCard->getNumber())
                {
                    if (0 == curCard->getNumber())
                    {
                        curCard->setNumber(difCard->getNumber());
                        difCard->setNumber(0);
                        
                        isMovedFlag = true;
                    }
                    else if (difCard->getNumber() == curCard->getNumber())
                    {
                        curCard->setNumber(2 * curCard->getNumber());
                        difCard->setNumber(0);
                        
                        score += curCard->getNumber();
                        
                        isMovedFlag = true;
                        
                        break;
                    }
                    else
                    {
                        break;
                    }
                }
            }
        }
    }
    
    *outScore = score;
    
    return isMovedFlag;
}

bool HelloWorld::doRight(int *outScore)
{
    log("doRight");
    
    bool isMovedFlag = false;
    int score = 0;
    
    for (int i = 0; i < MapSize; i++)
    {
        for (int j = MapSize-1; j > 0; j--)
        {
            CardSprite *curCard = m_pCardArray[i][j];
            for (int m = j-1; m >= 0; m--)
            {
                CardSprite *difCard = m_pCardArray[i][m];
                if (0 != difCard->getNumber())
                {
                    if (0 == curCard->getNumber())
                    {
                        curCard->setNumber(difCard->getNumber());
                        difCard->setNumber(0);

                        isMovedFlag = true;
                    }
                    else if (difCard->getNumber() == curCard->getNumber())
                    {
                        curCard->setNumber(2 * curCard->getNumber());
                        difCard->setNumber(0);
                        
                        score += curCard->getNumber();
                        
                        isMovedFlag = true;
                        
                        break;
                    }
                    else
                    {
                        break;
                    }
                }
            }
        }
    }
    
    *outScore = score;
    
    return isMovedFlag;
}

void HelloWorld::createCardSprite(Size aSize, int *outXMargin, int *outYMargin)
{
    int minSide = aSize.width > aSize.height ? aSize.height : aSize.width;
    int unitSize = (minSide - kCardSize) / MapSize;
    int xMargin = (aSize.width - (unitSize * MapSize)) / 2;
    int yMargin = (aSize.height - (unitSize * MapSize)) / 2;
    
    for (int i = 0; i < MapSize; i++)
    {
        for (int j = 0; j < MapSize; j++)
        {
            CardSprite *card = CardSprite::createCardSprite(
                        0, unitSize, unitSize, 
                        (unitSize*j) + xMargin, (unitSize*i) + yMargin);
            card->setAnchorPoint(Point(0.5f, 0.5f));
            this->addChild(card);
            m_pCardArray[MapSize-1-i][j] = card;
        }
    }
    
    *outXMargin = xMargin;
    *outYMargin = yMargin;
}

void HelloWorld::autoCreateCardNumber()
{
    int i = CCRANDOM_0_1()*MapSize;
    int j = CCRANDOM_0_1()*MapSize;
    
    if (0 != m_pCardArray[i][j]->getNumber())
    {
        this->autoCreateCardNumber();
    }
    else
    {
        m_pCardArray[i][j]->setNumber(CCRANDOM_0_1() < 0.5 ? 2 : 4);
        
        m_pCardArray[i][j]->setScale(0.0f, 0.0f);
        ScaleTo *scale = ScaleTo::create(0.35f, 1.0f, 1.0f, 1.0f);
        m_pCardArray[i][j]->runAction(scale);
    }
}

bool HelloWorld::isGameOver()
{
    bool isNotDeadFlag = false;
    for (int i = 0; i < MapSize; i++)
    {
        int lastIndex = -1;
        for (int j = 0; j < MapSize; j++)
        {
            if (0 == m_pCardArray[i][j]->getNumber())
            {
                isNotDeadFlag = true;
                break;
            }
            else
            {
                if (-1 == lastIndex)
                {
                    lastIndex = j;
                }
                else if(m_pCardArray[i][j]->getNumber() == m_pCardArray[i][lastIndex]->getNumber())
                {
                    isNotDeadFlag = true;
                    break;
                }
                else
                {
                    lastIndex = j;
                }
            }
        }
    }
    if (isNotDeadFlag)
    {
        return !isNotDeadFlag;
    }
    
    for (int j = 0; j < MapSize; j++)
    {
        int lastIndex = -1;
        for (int i = 0; i < MapSize; i++)
        {
            if (0 == m_pCardArray[i][j]->getNumber())
            {
                isNotDeadFlag = true;
                break;
            }
            else
            {
                if (-1 == lastIndex)
                {
                    lastIndex = i;
                }
                else if(m_pCardArray[i][j]->getNumber() == m_pCardArray[lastIndex][j]->getNumber())
                {
                    isNotDeadFlag = true;
                    break;
                }
                else
                {
                    lastIndex = i;
                }
            }
        }
    }
    
    return !isNotDeadFlag;
}
