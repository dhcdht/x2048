#ifndef __HELLOWORLD_SCENE_H__
#define __HELLOWORLD_SCENE_H__

#include "cocos2d.h"

#include "cardsprite.h"

#define MapSize 4

class HelloWorld : public cocos2d::Layer
{
public:
    // there's no 'id' in cpp, so we recommend returning the class instance pointer
    static cocos2d::Scene* createScene();

    // Here's a difference. Method 'init' in cocos2d-x returns bool, instead of returning 'id' in cocos2d-iphone
    virtual bool init();  
    
    // implement the "static create()" method manually
    CREATE_FUNC(HelloWorld);

    virtual bool onTouchBegan(cocos2d::Touch *touch, cocos2d::Event *unused_event);
    virtual void onTouchEnded(cocos2d::Touch *touch, cocos2d::Event *unused_event);

    bool doUp(int *outScore);
    bool doDown(int *outScore);
    bool doLeft(int *outScore);
    bool doRight(int *outScore);
    
    void createCardSprite(cocos2d::Size aSize, int *outXMargin, int *outYMargin);
    void autoCreateCardNumber();
    bool isGameOver();

private:
    int beginX, beginY, endX, endY;
    CardSprite *m_pCardArray[MapSize][MapSize];
    
    int m_iScore;
    cocos2d::LabelTTF *m_pLabelScore;
};

#endif // __HELLOWORLD_SCENE_H__
