#include "cardsprite.h"

USING_NS_CC;

CardSprite *CardSprite::createCardSprite(int aNumber, int aWidth, int aHeight, float aSpriteX, float aSpriteY)
{
    CardSprite* instance = new CardSprite();
    if (instance && instance->init())
    {
        instance->autorelease();
        instance->privateInit(aNumber, aWidth, aHeight, aSpriteX, aSpriteY);
        
        return instance;
    }
    
    CC_SAFE_DELETE(instance);
    return NULL;
}

bool CardSprite::init()
{
    if (!Sprite::init())
    {
        return false;
    }
    
    m_iNumber = -1;
    
    m_pLayerColor = LayerColor::create(Color4B::BLACK);
    this->addChild(m_pLayerColor);
    
    m_pLabelNumber = LabelTTF::create();
    m_pLayerColor->addChild(m_pLabelNumber);
    
    return true;
}

int CardSprite::getNumber()
{
    return m_iNumber;
}

void CardSprite::setNumber(int aNumber)
{
    if (aNumber == m_iNumber)
    {
        return;
    }
    
    bool isAnimate = true;
    if (0 == m_iNumber)
    {
        isAnimate = false;
    }
    m_iNumber = aNumber;
    
    if (aNumber)
    {
        m_pLabelNumber->setString(String::createWithFormat("%i", m_iNumber)->getCString());
        
        float percent = (log(m_iNumber) / 10.0f);
        float opacity = 200 + percent * (255 - 200);
        int blue = 100 + percent * (255 - 100);
        
        if (isAnimate)
        {
            TintTo *tint = TintTo::create(0.35f, 100, 100, blue);
            FadeTo *fade = FadeTo::create(0.35f, opacity);
            m_pLayerColor->runAction(tint);
            m_pLayerColor->runAction(fade);
        }
        else
        {
            m_pLayerColor->setColor(Color3B(100, 100, blue));
            m_pLayerColor->setOpacity(opacity);
        }
    }
    else 
    {
        m_pLabelNumber->setString("");
        m_pLayerColor->setColor(Color3B(25, 25, 50));
        m_pLayerColor->setOpacity(30);
    }
}

void CardSprite::privateInit(int aNumber, int aWidth, int aHeight, float aSpriteX, float aSpriteY)
{
    this->setPosition(aSpriteX+aWidth/2, aSpriteY+aHeight/2);
    this->setContentSize(Size(aWidth, aHeight));
    
    m_pLayerColor->setPosition(2, 2);
    m_pLayerColor->setContentSize(Size(aWidth-4, aHeight-4));
    
    m_pLabelNumber->setFontSize(60);
    m_pLabelNumber->setPosition(aWidth/2, aHeight/2);
    m_pLabelNumber->setFontFillColor(Color3B::BLACK);
    
    this->setNumber(aNumber);
}
